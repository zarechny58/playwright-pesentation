const {test, expect} = require('@playwright/test');

test.describe('parallelization inside single @file', () => {
    test('first test', async ({
                                  page
                              }) => {
        await page.goto('/');
        const title = page.locator('.navbar__inner .navbar__title');
        //await page.waitForTimeout(5000);
        await expect(title).toHaveText('Playwright');

    });
    test('second test @test2', async ({
                                   page
                               }) => {
        await page.goto('/');
        const menu = page.locator('.navbar__items:nth-child(1)');
        //await page.waitForTimeout(5000);
        await expect(menu).toContainText('Docs');

    });
});

