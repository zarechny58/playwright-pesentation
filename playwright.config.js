// playwright.config.js
// @ts-check

/** @type {import('@playwright/test').PlaywrightTestConfig} */

const config = {
    reporter: 'allure-playwright', use: {
        baseURL: 'https://playwright.dev', screenshot: 'only-on-failure', video: 'on-first-retry'
    }, projects: [

        {
            name: 'Chromium', use: {
                browserName: 'chromium', viewport: {
                    width: 1920, height: 1080, headless: false
                },
            }
        },

        {
            name: 'Firefox', use: {browserName: 'firefox', headless: false},
        },

        {
            name: 'WebKit', use: {browserName: 'webkit', headless: false},
        },],
};

module.exports = config;
